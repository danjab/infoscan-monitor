﻿using System;
using Microsoft.Extensions.Configuration;
using Monitor.Services;

namespace Monitor
{
    public class Program
    {
        static void Main()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = builder.Build();

            var examinationApiRequest = new ExaminationApi(configuration).CheckIfExaminationApiIsAlive();


            if (examinationApiRequest == null)
                throw new Exception("Cannot get internal web api token!");

            Console.WriteLine(
                $"Examination API status: {(examinationApiRequest.IsSuccessful ? "online" : "offline")} ({examinationApiRequest.StatusCode})");
        }

        
    }
}
