﻿namespace Monitor.Helpers
{
    public class ResponseHelper
    {
        public int StatusCode { get; set; }
        public bool IsSuccessful { get; set; }
    }
}
