﻿using System.Threading.Tasks;
using RestSharp;

namespace Monitor.Helpers
{
    public static class RestSharpClientExt
    {
        public static Task<IRestResponse> GetResponseContentAsync(this IRestClient theClient, IRestRequest theRequest)
        {
            var tcs = new TaskCompletionSource<IRestResponse>();
            theClient.ExecuteAsync(theRequest, response => { tcs.SetResult(response); });
            return tcs.Task;
        }
    }
}
