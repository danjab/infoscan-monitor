﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Monitor.Helpers;
using Newtonsoft.Json;
using RestSharp;

namespace Monitor.Services
{
    public class ExaminationApi
    {
        private readonly IConfigurationRoot _config;

        public ExaminationApi(IConfigurationRoot config)
        {
            _config = config;
        }


        public ResponseHelper CheckIfExaminationApiIsAlive()
        {
            var token = GetAdministrationInternalWebApiToken();
            if (string.IsNullOrWhiteSpace(token))
                return null;

            IRestClient client = new RestClient(_config["KeycloakAuthentication:ExaminationApiHost"]);

            IRestRequest request = new RestRequest
            {
                Method = Method.GET,
                Resource = _config["KeycloakAuthentication:RequestResource"]
            };
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", token);

            var response = new RestResponse();
            Task.Run(async () => { response = await client.GetResponseContentAsync(request) as RestResponse; }).Wait();


            return new ResponseHelper
            {
                IsSuccessful = response.IsSuccessful,
                StatusCode = (int) response.StatusCode
            };
        }


        #region Internal WebApi Token

        private string GetAdministrationInternalWebApiToken()
        {
            IRestClient client = new RestClient(_config["KeycloakAuthentication:Host"]);

            IRestRequest loginRequest = new RestRequest
            {
                Method = Method.POST,
                Resource = _config["KeycloakAuthentication:AdministrationInternalWebApiTokenRequestUrl"],
            };

            var basic = Base64Helper.Base64Encode(
                $"{_config["KeycloakAuthentication:KeycloakAdministrationInternalWebApiClient"]}:{_config["KeycloakAuthentication:KeycloakAdministrationInternalWebApiSecret"]}");


            loginRequest.AddHeader("Authorization", $"Basic {basic}");
            loginRequest.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            loginRequest.AddParameter("grant_type", "client_credentials");

            var response = new RestResponse();
            Task.Run(async () => { response = await client.GetResponseContentAsync(loginRequest) as RestResponse; })
                .Wait();
            var result = JsonConvert.DeserializeObject<dynamic>(response.Content);

            return result != null ? $"Bearer {result.access_token}" : null;
        }


        #endregion


    }
}
